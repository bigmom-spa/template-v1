# For not create-react-app setup

1. Copy src into working folder.
2. If package.json exists, copy relevant parts into it. If not, copy the whole file.
3. <code>npm install</code>
4. Ensure page has element with id="root", or modify it in index.tsx
5. If required, copy .eslintrc.json and tsconfig.json and modify as per project requirements.
6. Ensure that project copied to routes all non-api get routes to this SPA.

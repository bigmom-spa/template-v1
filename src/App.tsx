import React, { ReactElement } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import TestRouter from './pages/test/Router'
import TestSWR from './pages/test/SWR'
import TestFormik from './pages/test/Formik'
import TestApiRoute from './pages/test/APIRoute'
import NotFound from './pages/error/NotFound'

function App (): ReactElement {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/test/router">
          <TestRouter />
        </Route>
        <Route exact path="/test/swr">
          <TestSWR />
        </Route>
        <Route exact path="/test/formik">
          <TestFormik />
        </Route>
        <Route exact path="/test/api-route">
          <TestApiRoute />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </div>
  )
}

export default App

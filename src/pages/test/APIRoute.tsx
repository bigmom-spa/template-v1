import React, { ReactElement } from 'react'
import { getApiRoute, getApiUrl, getBaseApiUrl } from '../../lib/routes'
import { Link } from 'react-router-dom'

export default function APIRoute (): ReactElement {
  return (
    <div>
      <h1>Test API Route function</h1>
      <p>Okay this might be a misleading but this doesnt actually test any urls.<br />
      It just tests if the /lib/routes.ts functions work.</p>
      <ul>
        <li>getBaseApiUrl(): {getBaseApiUrl()}</li>
        <li>getApiUrl(&apos;/dashboard&apos;): {getApiUrl('/dashboard')}</li>
        <li>getApiRoute(&apos;home&apos;): {getApiRoute('home')}</li>
      </ul>
      <p><Link to="/">Home</Link></p>
    </div>
  )
}

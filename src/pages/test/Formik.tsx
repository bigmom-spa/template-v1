import { useFormik } from 'formik'
import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'
import apiSetFieldError from '../../lib/Form/apiSetFieldError'

export default function Formik (): ReactElement {
  const formik = useFormik({
    initialValues: {
      email: ''
    },
    onSubmit: (values, {
      setFieldError
    }) => {
      const mockResponse: { data: any } = {
        data: {
          errors: {
            email: 'Email has already been taken.'
          }
        }
      }

      apiSetFieldError(mockResponse.data.errors, setFieldError)
    }
  })

  return (
    <div>
      <h1>Formik test</h1>
      <form onSubmit={formik.handleSubmit}>
        <input type="email" name="email" placeholder="Email" onBlur={formik.handleBlur} onChange={formik.handleChange} />
        <p>{typeof formik.errors.email !== 'undefined' ? `error: ${formik.errors.email}` : <></>}</p>
        <button type="submit">Submit</button>
      </form>
      <p><Link to="/">Home</Link></p>
    </div>
  )
}

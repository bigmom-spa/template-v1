import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'

export default function Router (): ReactElement {
  return (
    <div>
      <h1>Router is working!</h1>
      <Link to="/">Home</Link>
    </div>
  )
}

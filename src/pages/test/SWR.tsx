import React, { ReactElement } from 'react'
import useSWR from 'swr'
import getIsLoading from '../../lib/SWR/getIsLoading'
import { Link } from 'react-router-dom'

export default function SWR (): ReactElement {
  const { data, error } = useSWR('https://run.mocky.io/v3/42a871c9-f059-4381-ad99-867f3e98608d')
  const isLoading = getIsLoading(data, error)

  if (isLoading) return <div>Loading...</div>
  if (typeof error !== 'undefined') return <div>Error!</div>

  return (
    <div>
      <h1>SWR Test</h1>
      <p>If you see something below, then it&apos;s working.</p>
      <textarea>{JSON.stringify(data)}</textarea>
      <p><Link to="/">Home</Link></p>
    </div>
  )
}

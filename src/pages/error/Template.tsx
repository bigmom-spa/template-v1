import React, { ReactElement } from 'react'

export interface ErrorPageProps {
  code: number
  message?: string
}

export default function ErrorTemplate ({ code, message }: ErrorPageProps): ReactElement {
  return (
    <div>
      <p>Error {code}</p>
      <p>{message}</p>
    </div>
  )
}

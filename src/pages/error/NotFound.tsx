import React, { ReactElement } from 'react'
import ErrorTemplate from './Template'

export default function NotFound ({ message }: {message?: string}): ReactElement {
  return (
    <ErrorTemplate code={404} message={typeof message !== 'undefined' ? message : 'Not found'} />
  )
}

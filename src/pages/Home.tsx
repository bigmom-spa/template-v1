import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'

export default function Home (): ReactElement {
  return (
    <div>
      <h1>Hello World!</h1>
      <p><Link to="/test/router">Test Router</Link></p>
      <p><Link to="/test/swr">Test SWR</Link></p>
      <p><Link to="/test/formik">Test Formik</Link></p>
      <p><Link to="/test/api-route">Test API Routes</Link></p>
    </div>
  )
}

export const API_URL = 'http://fake.url/api'

export async function API_FETCHER (resource: any, init: any): Promise<any> {
  const res = await fetch(resource, init)

  if (!res.ok) {
    const error = new Error('An error occured while fetching the data.') as APIError
    error.info = await res.json()
    error.status = res.status
    throw error
  }

  return await res.json()
}

export interface APIError extends Error {
  info: Promise<any>
  status: number
}

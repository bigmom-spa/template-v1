export default function apiSetFieldError (
  errors: Record<string, string>,
  setFieldError: (field: string, message: string | undefined) => void
): void {
  Object.keys(errors).forEach((key: string) => {
    setFieldError(key, errors[key])
  })
}

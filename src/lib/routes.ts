import { API_URL } from './defaults'

const baseApiUrl = API_URL

export function getBaseApiUrl (): string {
  if (typeof baseApiUrl === 'undefined') return ''
  return baseApiUrl
}

export const apiRoute: Record<string, string> = {
  home: '/'
}

export function getApiUrl (path: string): string {
  return getBaseApiUrl() + path
}

export function getApiRoute (route: string): string {
  return getApiUrl(apiRoute[route])
}

import React from 'react'
import { render, screen } from '@testing-library/react'
import Home from './pages/Home'

test('Renders Hello World! page', () => {
  render(<Home />)
  const linkElement = screen.getByText(/Hello World!/i)
  expect(linkElement).toBeInTheDocument()
})
